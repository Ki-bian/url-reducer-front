import React, { useState } from 'react';
import axios from 'axios';
import gconf from './configs/gconf';

const styles = {
  container: {
    position: 'relative',
    top: '25vh',
    fontSize: '4vh',
  },
  inputs: {
    verticalAlign: 'middle',
    fontSize: '2vh',
    fontWeight: 'bold',
    marginTop: '1vh',
    padding: '3vh 5vh 3vh 5vh',
    width: '40vh',
  },
  button: {
    fontSize: '4vh',
    padding: '1vh 2vh 1vh 2vh',
    fontWeight: 'bold',
    marginTop: '1.5vh',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  a: {
    marginTop: '2vh',
  },
};

const App = () => {
  const [originalUrl, setOriginalUrl] = useState('');
  const [urlReduced, setUrlReduced] = useState('');
  const [shortUrlBase, setShortUrlBase] = useState('');

  const handleChange = (event) => {
    if (event.target.name === 'url')setOriginalUrl(event.target.value);
    if (event.target.name === 'shortUrlBase') setShortUrlBase(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const { data } = await axios.post(`${gconf.server}/api/urls`, {
        originalUrl,
        shortUrlBase,
      });
      setUrlReduced(data.urlReduced);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <>
      <div style={{ ...styles.form, ...styles.container }}>
        <h1>Url reducer</h1>
        <form style={styles.form} onSubmit={handleSubmit}>
          <input style={styles.inputs} type="text" name="url" placeholder="your url ..." onChange={handleChange} required />
          <input style={styles.inputs} type="text" name="shortUrlBase" placeholder="your short url base..." onChange={handleChange} required />
          <input style={styles.button} type="submit" value="Envoyer" />
        </form>
        <a href={originalUrl} style={styles.a}>
          {urlReduced}
        </a>
      </div>
    </>
  );
};

export default App;
